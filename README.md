# PIPELINE PHPSTAN

Ejecutar el archivo execute-phpstan.sh, chequear los parámetros que se pasan

## Material de apoyo

Variable de entorno bitbucket
[https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/](https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/)

API Referencia
[https://developer.atlassian.com/bitbucket/api/2/reference/resource/](https://developer.atlassian.com/bitbucket/api/2/reference/resource/)

## Instalar composer
[https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos)

## Ejecutar para instalar estos paquetes en el servidor donde se correrá el pipeline
```bash
sudo apt install php
sudo apt-get install php-curl
composer install
```

## Contributing
Pull requests son bienvenidos, pendientes de los roles y permisos que necesiten la función Lambda.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)