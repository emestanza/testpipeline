<?php

define("API_BITBUCKET_BASE_URL", 'https://api.bitbucket.org/2.0/repositories/emestanza/testpipeline');
define("BITBUCKET_USER_CREDENTIALS", 'emestanza:Strtok44682');


$buildNr= $argv[1];

$cURLConnection = curl_init();

// Diff entre branch origen y destino
// Referencia: https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D/pullrequests/%7Bpull_request_id%7D/diff
curl_setopt($cURLConnection, CURLOPT_URL, API_BITBUCKET_BASE_URL."/pullrequests/".$buildNr."/diff");
curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($cURLConnection, CURLOPT_FOLLOWLOCATION, true);

$diffList = curl_exec($cURLConnection);
curl_close($cURLConnection);

$diffArr = explode("\n", $diffList);
$diffArr = array_filter($diffArr, "onlyDiff");

//var_dump($your_array); die();

foreach($diffArr as $diffLine) {

    $fileArr = explode("b/", $diffLine);

    if (sizeof($fileArr) == 2) {

        $fileName = $fileArr[1];
        $output = null;
        echo 'phpstan analyse '.$fileName;

        // ejecuto phpstan
        exec("vendor/bin/phpstan analyse ".$fileName." --level 1", $output);

        ///////////obtengo los comentarios previos
        // https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D/pullrequests/%7Bpull_request_id%7D/comments#get
        $cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, API_BITBUCKET_BASE_URL."/pullrequests/".$buildNr."/comments");
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($cURLConnection, CURLOPT_FOLLOWLOCATION, true);

        $commentList = curl_exec($cURLConnection);
        curl_close($cURLConnection);

        $commentList = json_decode($commentList, true);
        $moreComments = isset($commentList["next"])?$commentList["next"]:false;

        $commentList = array_map("getInline", $commentList["values"]); 
        $commentList = array_unique($commentList);

        while($moreComments) {

            $cURLConnection = curl_init();
            curl_setopt($cURLConnection, CURLOPT_URL, $moreComments);
            curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($cURLConnection, CURLOPT_FOLLOWLOCATION, true);

            $commentPageList = curl_exec($cURLConnection);
            curl_close($cURLConnection);

            $commentPageList = json_decode($commentPageList, true);
            $moreComments = isset($commentPageList["next"])?$commentPageList["next"]:false;
            $commentPageList = array_map("getInline", $commentPageList["values"]); 

            $commentList = array_merge($commentList, $commentPageList);
            $commentList = array_unique($commentList);
        }


        foreach($output as $result) {

            $aux = trim($result);
            $auxArr = explode(" ", $aux);
            

            if (is_numeric($auxArr[0])) {
                
                //var_dump($fileName.",".intval($auxArr[0]));

                if (!in_array($fileName.",".intval($auxArr[0]), $commentList)){
                    
                    $msg = trim(substr($aux, strlen($auxArr[0])));

                    $postRequest = array(
                        'content' => array("raw" => $msg),
                        'inline' => array("to" => intval($auxArr[0]), "path" => $fileName)
                    );
                    
                    // Creo el comentario 
                    // https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D/pullrequests/%7Bpull_request_id%7D/comments#post
                    $cURLConnection = curl_init(API_BITBUCKET_BASE_URL.'/pullrequests/'.$buildNr.'/comments');
                    curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
                    curl_setopt($cURLConnection, CURLOPT_POST, 1);
                    curl_setopt($cURLConnection, CURLOPT_USERPWD, BITBUCKET_USER_CREDENTIALS);
                    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                    ));
                    
                    $apiResponse = curl_exec($cURLConnection);
                    curl_close($cURLConnection);
                    
                }
                else{
                    //var_dump("ya esta hecho el comentario");
                }
                
            }

        }

    }

}

/**
 * Obtengo la relacion de archivos provenientes de un diff
 */
function onlyDiff($var)
{
    // returns whether the input integer is odd
    return strpos($var, "diff") !== false && strpos($var, ".php") !== false;
}

/**
 * Obtengo la relación <archivo>,<linea> 
 */
function getInline($var){
    return $var["inline"]["path"].",".$var["inline"]["to"];
}