#!/bin/bash

# $1 => BITBUCKET_PR_ID
# $2 => BITBUCKET_REPO_SLUG
# $3 => BITBUCKET_BRANCH


cd /var/www/$2
echo 'Repositorio: '$2
echo "Git fetch"
git fetch
echo 'Checkout a '$3
git checkout $3
echo '----Git pull----'
git pull
echo '----Corriendo PHPSTAN----'
php /var/www/scripts/process.php $1

echo "Terminado";